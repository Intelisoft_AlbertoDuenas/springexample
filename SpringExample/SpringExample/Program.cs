﻿using Spring.Context;
using Spring.Context.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpringExample
{
    class Program
    {
        static void Main(string[] args)
        {
            IApplicationContext Context = ContextRegistry.GetContext();
            var myInstance = (BaseInstance)Context.GetObject("Instances3");
            Console.WriteLine(myInstance.Name + " " + myInstance.ID);
            Console.Read();
        }
    }
}

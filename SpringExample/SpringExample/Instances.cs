﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpringExample
{
    public class Instances : BaseInstance
    {
        public override string ToString()
        {
            return "I'm nothing " + ID + " " + Name;
        }
    }

    public class InstancesA : BaseInstance
    {
        public override string ToString()
        {
            return "I'm A " + ID + " " + Name;
        }
    }

    public class InstancesB : BaseInstance
    {
        public override string ToString()
        {
            return "I'm B " + ID + " " + Name;
        }
    }

    public abstract class BaseInstance
    {
        public string Guid { get; set; }
        public string Name { get; set; }
        public int ID { get; set; }
    }
}
